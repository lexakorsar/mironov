class SwParseJob <  Struct.new(:attach_id)
  def perform
    SwDbfFile.find(attach_id).parse_data
  end
end
