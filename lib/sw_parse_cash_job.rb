class SwParseCashJob <  Struct.new(:attach_id)
  def perform
    SwCashFiles.find(attach_id).parse_data
  end
end