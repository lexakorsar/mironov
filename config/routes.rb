Mironov::Application.routes.draw do

  root :to => 'main#index'

  get 'application/plan_create', :as => :plan_create
  get 'application/implementation_create', :as => :implementation_create

  devise_for :admin_users, ActiveAdmin::Devise.config do
    ActiveAdmin.routes(self)
  end

  devise_for :users do

    namespace :sweet do
      root :to => 'conference#index'
      get 'plan/next_day', :as => :plan_next_day
      get 'plan/prev_day', :as => :plan_prev_day

      get 'implementation/next_day', :as => :implementation_next_day
      get 'implementation/prev_day', :as => :implementation_prev_day

      get 'cash/next_day', :as => :cash_next_day
      get 'cash/prev_day', :as => :cash_prev_day

      get 'cashc/next_day', :as => :cashc_next_day
      get 'cashc/prev_day', :as => :cashc_prev_day

      get 'sold_done/index', :as => :sold_done

      match 'balance/' => 'balance#index'
      match 'cash/' => 'cash#index'
      match 'cashc/' => 'cashc#index'
      match 'implementation/' => 'implementation#index'
      match 'implementation/:id' => 'implementation#show', :as => :implementationshow
      match 'implementation/:id/all' => 'implementation#all', :as => :implementationall
      match 'plan/' => 'plan#index'
      match 'plan/:id' => 'plan#show', :as => :planshow
      match 'plan/:id/all' => 'plan#all', :as => :planall
      match 'conference/:id' => 'conference#show', :as => :conferenceshow
      match 'conference/' => 'conference#index'
      match 'set_date/' => 'application#save_cur_date', :as => :set_date

    end

    get 'plan/next_day', :as => :plan_next_day
    get 'plan/prev_day', :as => :plan_prev_day

    get 'implementation/next_day', :as => :implementation_next_day
    get 'implementation/prev_day', :as => :implementation_prev_day

    get 'cash/next_day', :as => :cash_next_day
    get 'cash/prev_day', :as => :cash_prev_day

    get 'cashc/next_day', :as => :cashc_next_day
    get 'cashc/prev_day', :as => :cashc_prev_day

    get 'sold_done/index', :as => :sold_done

    match 'balance/' => 'balance#index'
    match 'cash/' => 'cash#index'
    match 'cashc/' => 'cashc#index'
    match 'implementation/' => 'implementation#index'
    match 'implementation/:id' => 'implementation#show', :as => :implementationshow
    match 'implementation/:id/all' => 'implementation#all', :as => :implementationall
    match 'plan/' => 'plan#index'
    match 'plan/:id' => 'plan#show', :as => :planshow
    match 'plan/:id/all' => 'plan#all', :as => :planall
    match 'conference/:id' => 'conference#show', :as => :conferenceshow
    match 'conference/' => 'conference#index'
    match 'set_date/' => 'application#save_cur_date', :as => :set_date

  end

end
