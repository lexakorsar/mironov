class AddSw < ActiveRecord::Migration
  def change
    create_table :sw_dbf_files do |t|

      t.attachment :plan
      t.attachment :implementation

      t.timestamps
    end

    create_table :sw_cash_files do |t|

      t.attachment :cash
      t.attachment :cashc
      t.attachment :balance

      t.timestamps
    end
  end
end
