class AddSwAssortments < ActiveRecord::Migration
  def change
    create_table :sw_assortments do |t|
      t.string :name
      t.integer :old_id
      t.integer :level

      t.timestamps
    end
  end
end
