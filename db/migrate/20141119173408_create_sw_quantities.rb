class CreateSwQuantities < ActiveRecord::Migration
  def change
    create_table :sw_quantities do |t|
      t.integer :kol
      t.date :date

      t.timestamps
    end
  end
end
