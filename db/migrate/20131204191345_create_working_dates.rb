class CreateWorkingDates < ActiveRecord::Migration
  def change
    create_table :working_dates do |t|
      t.string :work_date
      t.date :work_date_value

      t.timestamps
    end
  end
end
