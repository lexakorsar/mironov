class Sweet::SoldDoneController < Sweet::BaseController
  def index
    #implementation

    year = Time.now.year - 2
    @implementations = SwImplementation.where('N = ? AND YEAR(DAT) >= ?', 0, year).order("DAT DESC")

    @month_implementations_arr = []
    Time.now.year.to_i.downto (Time.now.year - 1).to_i do |year|
      if year == Time.now.year
        months = Time.now.month
      else
        months = 12
      end
      months.downto 1 do |month|
        if year == Time.now.year && month == Time.now.month
          @day = @workin_date.work_date_value.day
          @dat = @workin_date.work_date_value
          @nowday = @dat.day
          impl = SwImplementation.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, year, month, @dat.day).first
          #raise impl.inspect
          while impl == nil do
            impl = SwImplementation.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, @dat.year, @dat.month, @dat.day).first
            @nowday = @dat.day
            @dat = @dat - 1.day
            @day = @day - 1
          end

          @month_implementations_arr << impl.id if impl
        else
          @day = Time.new(year, month, 1).end_of_month.day
          @month_implementations_arr << SwImplementation.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, year, month, @day).first.id if SwImplementation.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, year, month, @day).first
        end

        @month_implementations = SwImplementation.where('id IN (?)',@month_implementations_arr).order("DAT DESC")
      end
    end

    #plan

    year = 2013
    @plans = SwAsrt.where('N = ? AND YEAR(DAT) >= ?', 0, year).order("DAT DESC")
    @month_plans = []
    @days_plans_arr = []
    @planday = @workin_date.work_date_value
    #@dat = (@workin_date.work_date_value)
    #@planday = (Time.now - 1.day)
    @nowday = @planday
    pl = SwAsrt.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, @planday.year, @planday.month, @planday.day).first
    while pl == nil do
      @planday = @planday -1.day
      pl = SwAsrt.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, @planday.year, @planday.month, @planday.day).first
      @nowday = @planday

    end
    Time.now.year.to_i.downto 2013 do |year|
      if year == Time.now.year
        months = Time.now.month
      else
        months = 12
      end
      months.downto 1  do |month|
        @day = Time.new(year,month,1).end_of_month.day
        @month_plans << SwAsrt.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, year, month, @day).first.id if SwAsrt.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, year, month, @day).first
        @days_plans_arr << SwAsrt.where('N = ? AND YEAR(DAT) = ? AND MONTH(DAT) = ? AND DAY(DAT) = ?', 0, year, month, @nowday.day).first

      end
    end
    @month_plans << SwAsrt.where('N = ?', 0).last.id
    @month_plans = SwAsrt.where('id IN (?)', @month_plans).order("DAT DESC")

  end
end
