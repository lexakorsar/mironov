class WorkingDate < ActiveRecord::Base
  acts_as_singleton
  attr_accessible :work_date, :work_date_value
end
